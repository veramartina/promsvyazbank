require: patterns.sc
    module = common

require: common.js
    module = common

require: number/number.sc
    module = common

require: offtopic/offtopic.sc
    module = common

require: dictionaries/offtopic.yaml
    var = specialOfftopic

require: dictionaries/answers.yaml
    var = specialSwitch

require: catchAll/catchAll.sc
    module = common

require: general.sc
require: payments.sc
require: services.sc

init:
    bind("postProcess", function($context) {
        var $response = $context.response, $temp = $context.temp, $session = $context.session, $request = $context.request;

        //Обработка переносов строк и звездочек для для нормального вывода в виджете.

        log(JSON.stringify($request))
        if($request.channelType == 'chatwidget' && $response.replies){
            for (var i = 0; i < $response.replies.length; i++) {
                var r = $response.replies[i];
                if (r.type === 'text') {
                    r.text = r.text.replace(/(\n)+/g, '\n\n');
                    r.text = r.text.replace(/(\*(\d*)(\*\d+)?\#)/g, '```$&```');
                }
            }
        }
    })

theme: /

    state: Start
        q!: * *start
        script:
            $session = {}
            $client = {}
            $temp = {}
            $response = {}
        a: Здравствуйте! Я электронный помощник Промсвязьбанка. Чем я могу Вам помочь?


    state: reset
        q: reset
        script:
            $session = {}
            $temp = {}
            $response = {}
        go: /
        

    state: OutOfScope
        a: Вероятно, ваш вопрос не в моей компетенции.